'use strict';

var through = require('through2');
var path = require('path');
var File = require('vinyl');

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

module.exports = function(file, opt) {
    if (!file) {
        throw new Error('gulp-index-dir: Missing file option');
    }
    opt = opt || {};

    if (typeof file === 'string') {
        fileName = file;
    } else if (typeof file.path === 'string') {
        fileName = path.basename(file.path);
    } else {
        throw new Error('gulp-index-dir: Missing path in file options');
    }

    var fileNames = [];
    var lastFile;
    var fileName;

    function bufferContents(file, enc, cb) {
        if (file.isNull()) {
            cb();
            return;
        }

        if (file.isStream()) {
            this.emit('error', new Error('gulp-index-dir: Streaming not supported'));
            cb();
        }

        lastFile = file;

        fileNames.push(file.path);
        this.push(file);
        cb();
    }

    function endStream(cb) {
        if (!lastFile) {
            cb();
            return;
        }

        if (typeof file === 'string') {
          var f = lastFile.clone({contents: false});
          f.path = path.join(lastFile.base, file);
        } else {
          var f = new File(file);
        }

        var s = "<ul c style=\"list-style: none;\" lass=\"index__dir index__root_dir\">";
        fileNames.forEach((e) => {
            const nameps = e.split("/src/"); // very shoddy way to get the relative path
            const name = nameps[nameps.length-1];
            s += "<li><img src=\"https://twemoji.maxcdn.com/72x72/1f4c4.png\" alt=\"File\" style=\"height: 1.2em\"><a class=\"index__file\" href=\"" + name + "\">" + path.basename(e)
            .split('.')[0].replace(/_/g, ' ')
            .replace(/[ \t\f]+/g, ' ')
            .replace(/( |^)([a-z])/g, (m, p1, p2) => {
              return ' ' + p2.toUpperCase();
            }) + "</li>";
        });
        f.contents = Buffer.from(s + "</ul>");
        this.push(f);
        cb();
    }

    return through.obj(bufferContents, endStream);
}
