//$PWD/node_modules/gulp/bin/gulp.js $@; exit $?
// ^ Works similar to a shebang line (#!node_modules/gulp/bin/gulp.js)
const gulp       = require('gulp');
const posthtml   = require('gulp-posthtml');
const postcss    = require('gulp-postcss');
const sourcemaps = require('gulp-sourcemaps');
const concat     = require('gulp-concat');
const markdownit = require('gulp-markdownit');
const modifyFile = require('gulp-modify-file');
const mathJax    = require('gulp-mathjax-node');
const hljs       = require('highlight.js');
const path       = require('path');
const fs         = require('fs');

const postcssPlugins = [
  require('precss')({}),
  require('cssnano')({}),
];

const postcssOptions = {
  from: undefined,
};

const posthtmlPlugins = [
  require('posthtml-doctype')({ doctype: "HTML 5" }),
  require('posthtml-head-elements')({ headElements: 'html-head.json' }),
  require('posthtml-modules')({}),
  require('posthtml-favicons')({
    configuration: {
      appName        : "Ma_124's misc pages",
      appDescription : "",

      developerName  : 'Ma_124',
      developerURL   : 'https: //gitlab.com/Ma_124',

      background     : '#fff',
      theme_color    : '#fff',
    },
    outDir: "./public",
  }),
  require('posthtml-postcss')(postcssPlugins, postcssOptions),
  require('posthtml-minifier')({
    collapseBooleanAttributes : true,
    collapseWhitespace        : true,
    conservativeCollapse      : true,
    html5                     : true,
    removeComments            : true,
    removeEmptyAttributes     : true,
    sortAttributes            : true,
    sortClassName             : true,
  }),
];

const posthtmlOptions = {};

const markdownitConfig = {
  highlight: function (str, lang) {
    if (lang && hljs.getLanguage(lang)) {
      try {
        return hljs.highlight(lang, str).value;
      } catch (__) {}
    }

    return '';
  },
  options       : {
    html        : true,
    linkify     : true,
    typographer : true,
  },
  plugins: [
    { plugin: require('markdown-it-task-lists'),        options: null },
    { plugin: require('markdown-it-sub'),               options: null },
    { plugin: require('markdown-it-sup'),               options: null },
    { plugin: require('markdown-it-footnote'),          options: null },
    { plugin: require('markdown-it-deflist'),           options: null },
    { plugin: require('markdown-it-container'),         options: null },
    { plugin: require('markdown-it-ins'),               options: null },
    { plugin: require('markdown-it-mark'),              options: null },
    { plugin: require('markdown-it-container'),         options: null },
    { plugin: require('markdown-it-anchor'),            options: null },
    { plugin: require('markdown-it-table-of-contents'), options: null },
    { plugin: require('markdown-it-emoji'),             options: { shortcuts: [], renderer: 'twemoji' } },
    { plugin: require('markdown-it-front-matter'),      options: (fm) => {} },
//  { plugin: require('../markdownit-embed'),           options: null },
  ],
}

const mathJaxConfig = {}

gulp.task('html', function () {
  return gulp.src(['src/**/*.html'])
    .pipe(posthtml(posthtmlPlugins, posthtmlOptions))
    .pipe(gulp.dest('./public'));
});

gulp.task('md', function () {
  return gulp.src(['src/**/*.md'])
    .pipe(markdownit(markdownitConfig))
    .pipe(mathJax(mathJaxConfig)) // TODO
    .pipe(require('./gulp_index_dir.js')("index.html")) // TODO
    .pipe(modifyFile((content, pathStr, file) => {
      return "<html><head>" + (path.basename(pathStr) == "index.html" ? "<base href=\"https://ma124.js.org/misc/\">" : "") + "<title>" + path.basename(pathStr)
        .split('.')[0].replace(/_/g, ' ')
        .replace(/[ \t\f]+/g, ' ')
        .replace(/( |^)([a-z])/g, (m, p1, p2) => {
          return ' ' + p2.toUpperCase();
        }) + "</title><posthtml-head-elements></posthtml-head-elements></head><body>" + content + "</body></html>";
    }))
    .pipe(posthtml(posthtmlPlugins, posthtmlOptions))
    .pipe(gulp.dest('./public'));
});

gulp.task('style', function () {
  return gulp.src(['style*.css'])
    .pipe(sourcemaps.init())
    .pipe(postcss(postcssPlugins))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./public'));
});

gulp.task('assets', function () {
  return gulp.src(['assets/**'])
    .pipe(gulp.dest('./public'));
});

// vim: ts=2:sw=2

