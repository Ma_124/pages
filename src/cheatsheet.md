<!--
Copyright 2013-2018 Benoit Schweblin, https://twitter.com/benweet

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

[[toc]]

# Headers

## Header 2

### Header 3



# Styling

*Emphasize* _emphasize_

**Strong** __strong__

==Marked text.==

~~Mistaken text.~~

> Quoted text.

H~2~O is a liquid.

2^10^ is 1024.



# Lists

- Item
  * Item
    + Item

1. Item 1
2. Item 2
3. Item 3

- [ ] Incomplete item
- [x] Complete item



# Links

A [link](http://example.com).

An image: ![Alt](https://ma124.js.org/favicon-32x32.png)

# Code

Some `inline code`.

```
// A code block
var foo = 'bar';
```

```javascript
// An highlighted block
var foo = 'bar';
```

# Tables

Item     | Value
-------- | -----
Computer | $1600
Phone    | $12
Pipe     | $1


| Column 1 | Column 2      |
|:--------:| -------------:|
| centered | right-aligned |



# Definition lists

Markdown
:  Text-to-HTML conversion tool

Authors
:  John
:  Luke



# Footnotes

Some text with a footnote.[^1]

[^1]: The footnote.



# Abbreviations

Markdown converts text to HTML.

*[HTML]: HyperText Markup Language


# Embed
@[yt:tLgttp5CdjQ]
@[snip:1793435]
@[gist:b21b3abfcb03630893bcfec6e5996a9c]
@[imgur:jbqK8MJ]



# LaTeX math

The Gamma function satisfying $\Gamma(n) = (n-1)!\quad\forall
n\in\mathbb N$ is via the Euler integral

$$\Gamma(z) = \int_0^\infty t^{z-1}e^{-t}dt\,.$$

