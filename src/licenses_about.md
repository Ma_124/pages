# `/l/**`, `/.netlify/functions/l/**` Handler

[[toc]]

*URL Schema:* `/l/<LICENSE>/<NAME>~<SITE>~<EMAIL>/<YEAR>`

## Allowed URLs

### `/l/<LICENSE>/<NAME>`
| Field   | Value       |
|---------|-------------|
| License | `<LICENSE>` |
| Name    | `<NAME>`    |
| Site    |             |
| EMail   |             |
| Year    | 2018        |

### `/l/<LICENSE>/<NAME>~<SITE>`
| Field   | Value       |
|---------|-------------|
| License | `<LICENSE>` |
| Name    | `<NAME>`    |
| Site    | `<SITE>`    |
| EMail   |             |
| Year    | 2018        |

### `/l/<LICENSE>/<NAME>~<SITE>~<MAIL>`
| Field   | Value       |
|---------|-------------|
| License | `<LICENSE>` |
| Name    | `<NAME>`    |
| Site    | `<SITE>`    |
| EMail   | `<MAIL>`    |
| Year    | 2018        |

### `/l/<LICENSE>/<NAME>~~<MAIL>`
| Field   | Value       |
|---------|-------------|
| License | `<LICENSE>` |
| Name    | `<NAME>`    |
| Site    |             |
| EMail   | `<MAIL>`    |
| Year    | 2018        |

### `/l/<LICENSE>/<NAME>/<YEAR>`
| Field   | Value       |
|---------|-------------|
| License | `<LICENSE>` |
| Name    | `<NAME>`    |
| Site    |             |
| EMail   |             |
| Year    | `<YEAR>`    |

### `/l/<LICENSE>/<NAME>~<SITE>/<YEAR>`
| Field   | Value       |
|---------|-------------|
| License | `<LICENSE>` |
| Name    | `<NAME>`    |
| Site    | `<SITE>`    |
| EMail   |             |
| Year    | `<YEAR>`    |

### `/l/<LICENSE>/<NAME>~<SITE>~<MAIL>/<YEAR>`
| Field   | Value       |
|---------|-------------|
| License | `<LICENSE>` |
| Name    | `<NAME>`    |
| Site    | `<SITE>`    |
| EMail   | `<MAIL>`    |
| Year    | `<YEAR>`    |

### `/l/<LICENSE>/<NAME>~~<MAIL>/<YEAR>`
| Field   | Value       |
|---------|-------------|
| License | `<LICENSE>` |
| Name    | `<NAME>`    |
| Site    |             |
| EMail   | `<MAIL>`    |
| Year    | 2018        |

## Allowed Year Formats

`2016`
: `2016-2019` (current year)

`@2016`
: `2016`

`2016-2017`
: `2016-2017`

