# HPM
## Password Generation Process

```mermaid
graph LR
subgraph ""
subgraph Input
MK(Master Key)
WWW(Site Name)
end
C(Counter)-->msg
msg(Message)-->MAC
MK--Key-->MAC
WWW-->msg
MAC(MAC)-->BaseN
end
BaseN(Base 89)-->QA{Quality Assurance}
QA--strong-->out[Output]
QA--weak-->rtry
rtry[Retry]--+1-->C

style out fill:Crimson
style subGraph1 opacity: 0
click MAC https://en.wikipedia.org/wiki/Message_authentication_code
click QA https://github.com/dropbox/zxcvbn
click BaseN https://docs.oracle.com/javase/9/docs/api/java/lang/Integer.html#toString-int-int- "Similar to Java's toString(i, radix)"
```
### Example
```mermaid
graph LR
subgraph ""
subgraph Input
MK(Master Key)
WWW(Site Name)
end
C(Counter)--0-->msg
msg(Message)--"gitlab.com;0"-->MAC
MK--S3cr3t-->MAC
WWW--gitlab.com-->msg
MAC(HMAC)--"38798a..."-->BaseN
end
BaseN(Base 89)--"BbZX3}BXJQ0n"-->QA{"Upper Case: ✓<br />Lower Case: ✓<br />Numbers: ✓<br />etc (see zxcvbn)"}
QA==strong==>out[Output]
QA--weak-->rtry
rtry[Retry]--+1-->C

style out fill:Crimson
style subGraph1 opacity: 0
click MAC https://en.wikipedia.org/wiki/Message_authentication_code
click QA https://github.com/dropbox/zxcvbn
click BaseN https://docs.oracle.com/javase/9/docs/api/java/lang/Integer.html#toString-int-int- "Similar to Java's toString(i, radix)"
```

## Gen 2
```mermaid
graph LR
subgraph ""
subgraph Input
MK(Master Key)
RC("Reset Counter")
WWW("Site Name<br />(including username)")
end

C(Internal Counter)-->msg
RC-->msg
msg("Message<br />[User␞Site␞RCounter␞IntCounter]")-->KDF
MK--Key-->KDF
WWW-->msg
KDF(PwKDF/HKDF)-->BaseN
end
BaseN(Base 89)-->QA{Output Constraints}
QA--accepted-->out[Output]
QA--not accepted-->rtry
rtry[Retry]--+1-->C

style out fill:Crimson
style subGraph1 opacity: 0
click MAC https://en.wikipedia.org/wiki/Message_authentication_code
click QA https://github.com/dropbox/zxcvbn
click BaseN https://docs.oracle.com/javase/9/docs/api/java/lang/Integer.html#toString-int-int- "Similar to Java's toString(i, radix)"
```

#### Cons \w vault-less
* can't generate password in low security environment 
	* against key loggers and phishing: special device which acts as an HID keyboard.
* 1FA vs with vault 2FA

#### Cons \w design
* Master Key: 16 bytes randomness (HKDF)

