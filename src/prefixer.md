auto prefix `#!/bin/sh` for .sh scripts if no shebang is present
`package foo; // import "github.com/..."` OR `package foo;` -> `package foo; import "gopkg.in/foo.v3"`
Auto prefix licenses with correct comment chars
trailing `\n`
`:GoFmt`

```ini
[[Prefixer]]
; but not if shebang already exists
; auto append \n after every shebang line found
Glob="*.sh"
Prefix="#!/bin/sh\n\n"

[[Replacer]]
; only if first line after comments and newlines is `package ...` 
Glob="*.go"
Replace="package (.*?) ((// import .*?\n)|(/\* import .*? \*/\n))"
; auto detect package path ($PWD - $GOPATH/src/
By="package {.Match[1]} // import gopkg.in/{.Config.User}/{.Match[1]}"

[[Prefixer]]
Prefix="(curl https://ma124.js.org/l/{.Config.License}.short.txt)\n\n"
```
