
# SPDX Spec 2.1 Overview
> This is not an official SPDX Specification. Portions herein have been reproduced from [SPDX® Specification 2.1][spec-2] found at [spdx.org](https://spdx.org). These portions are Copyright &copy; 2010­-2016 Linux Foundation and its Contributors, and are licensed under the [Creative Commons Attribution License 3.0 Unported][cc-by-3] by the Linux Foundation and its Contributors. All other rights are expressly reserved by Linux Foundation and its Contributors.

[[toc]]

Tag | Description | Example | Cardinality
----|-------------|---------|------------
`SPDXVersion`          | SPDX File Version                     | `SPDX-2.1` (*const*)              | 1
`DataLicense`          | License of the SPDX File              | [`CC0-1.0`][cc0-1] (*const*)      | 1
`SPDXID`               | [see][spdxid]                         |                                   |
`DocumentName`         | Name of this Document                 | `ubuntu-14.04`                    | 1
`DocumentNamespace`    | The URL to access the SPDX file       | [see][nsf]                        | 1
`ExternalDocReference` | Reference SPDX Element                | [see][reff]                       | *
`LicenseListVersion`   | The [license list][ll] version        | `3.3`                             | ?
`Creator`              | A creator (human, bot or organisation | [see][creatorf]                   | +
`Created`              | Creation date of this document        | `2018-12-17T19:00:12Z` (ISO 8601) | 1
`CreatorComment`       | A comment about the creation of this document | `<text>...</text>`        | ?
`DocumentComment`      | A comment about this document         | `<text>...</text>`                | ?
|
`PackageName`          | The name of the package               | `glibc`                           | 1
`PackageVersion`       | The version of the package            | `1.12.14` [SemVer][semver]        | ?
`PackageFileName`      | An actual file name for the package   | `glibc-2.11.1.tar.gz`             | ?
`PackageSupplier`      | The [distribution source][pkgsup]     | `Ma_124`                          | ?
`PackageOriginator`    | The package originator/author         | `Ma_124`                          | ?
`PackageDownloadLocation` | The package download location/VCS  | [see][downloadlocf]               | 1
`PackageHomePage`      | The homepage of this package	       | `https://ma124.js.org/`           | ?
`PacakgeLicenseDeclared` | The SPDX License of the package     | `GPL-3.0-or-later`                | 1

### SPDX ID
***TODO***

### Formats
#### Document Namespace Format
The Document Namespace  is just an URL with preferably the following format:
`http[s]://<Creator Url>/<Path>/<Document Name>-<UUID>`

`Creator URL`
: An URL which is publicly available.

`Path`
: Just a sub path.

`Document Name`
: The document name specified in `DocumentName`.

`UUID`
: An UUID

#### External Doc Reference Format
`DocumentRef­-<ID> <Document Namespace> <Checksum>`

`ID`
: An unique ID  for later reference.

`Document Namespace`
: The document name space to load as defined [here][nsf].

`Checksum`
: A [Checksum][sum].

#### Creator Format
`Person: <Name> (<EMail>)` 
`Organisation: <Name> (<EMail>)`
`Tool: <Name>`

#### Download Location Format
URL or a VCS Path:
`<VCS>+<Transport>://<Host Name>/<Path to Repo>/[@<Revision, Tag or Branch>][#<Subpath>]`

`git://gitlab.com/Ma_124/dotfiles`
`git+https://gitlab.com/Ma_124/dotfiles.git`
`git+ssh://gitlab.com/Ma_124/dotfiles.git`
`git+git://gitlab.com/Ma_124/dotfiles.git`
`git+git@gitlab.com/Ma_124/dotfiles.git`

`git+https://gitlab.com/Ma_124/dotfiles.git#.config/nvim/init.vim`

`git+https://gitlab.com/Ma_124/dotfiles.git@master`
`git+https://gitlab.com/Ma_124/fit.git@v1.0`
`git+https://gitlab.com/Ma_124/fit.git@bf6d0f385c5eb6780de1a3309427c909e26b2e46`

`git+https://gitlab.com/Ma_124/fit.git@v1.0#api/utils.go`

For [more examples][downloc-examples] with other VCSs see §3.7.5

#### Package Supplier
Same as [Creator Format][creatorf] but without the `Tool`.

#### Checksum Format
`<Algorithm ID>: <Hex Output>`

`Algorithm ID`
: This is recommended to be `SHA1`

[nsf]: #document-namespace-format
[reff]: #external-doc-reference-format
[creatorf]: #creator-format
[downloadlocf]: #download-location-format
[sum]: #checksum-format
[spdxid]: #spdx-id
[pkgsup]: #package-supplier
[ll]: https://spdx.org/licenses/
[downloc-examples]: https://spdx.org/spdx-specification-21-web-version#h.49x2ik5
[cc-by-3]: https://creativecommons.org/licenses/by/3.0/
[cc0-1]: https://creativecommons.org/publicdomain/zero/1.0/
[spec-2]: https://spdx.org/sites/cpstandard/files/pages/files/spdxversion2.1.pdf
[semver]: https://semver.org/ "Nice but not required"
