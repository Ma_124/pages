---
title: Very awesome WASM
---
# Very awesome WASM
Short compilation of the for *me* most relevant and mature WASM compilers from [appcypher/awsome-wasm-langs](https://github.com/appcypher/awesome-wasm-langs)

* [TypeScript](https://github.com/appcypher/awesome-wasm-langs#assemblyscript) :hatching_chick:
* [C#](https://github.com/appcypher/awesome-wasm-langs#csharp) :hatching_chick:
* [C++](https://github.com/appcypher/awesome-wasm-langs#cpp) :hatched_chick:
* [Forth](https://github.com/appcypher/awesome-wasm-langs#forth) :hatched_chick:
* [Go](https://github.com/appcypher/awesome-wasm-langs#go) :hatching_chick:
* [Java](https://github.com/appcypher/awesome-wasm-langs#java) :hatching_chick:
* [Idris](https://github.com/appcypher/awesome-wasm-langs#idris) :hatching_chick:
* [Kotlin/Native](https://github.com/appcypher/awesome-wasm-langs#kotlin) :hatching_chick:
* [PHP](https://github.com/appcypher/awesome-wasm-langs#php) :hatching_chick:
* [Python](https://github.com/appcypher/awesome-wasm-langs#python) :hatching_chick:
* [Prolog](https://github.com/appcypher/awesome-wasm-langs#prolog) :hatching_chick:
* [Ruby](https://github.com/appcypher/awesome-wasm-langs#ruby) :hatching_chick:
* [Rust](https://github.com/appcypher/awesome-wasm-langs#rust) :hatched_chick:
* [Scheme](https://github.com/appcypher/awesome-wasm-langs#scheme) :hatching_chick:

